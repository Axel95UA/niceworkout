package com.axelua.niceworkout;

import android.app.ActivityManager;
import android.app.Application;
import android.app.Service;
import android.content.Intent;

import com.axelua.niceworkout.config.AppComponent;
import com.axelua.niceworkout.config.AppModule;
import com.axelua.niceworkout.config.DaggerAppComponent;
import com.axelua.niceworkout.model.AlarmService;

/**
 * created by Alex Ivanov on 13.07.15.
 */
public class WorkoutApp extends Application {

    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        if (!isAlarmServiceRunning()) {
            startService(new Intent(this, AlarmService.class).addFlags(Service.START_STICKY));
        }
    }

    public static AppComponent getComponent() {
        return component;
    }

    private boolean isAlarmServiceRunning() {
        ActivityManager alarmManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : alarmManager.getRunningServices(Integer.MAX_VALUE)) {
            if (AlarmService.class.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
