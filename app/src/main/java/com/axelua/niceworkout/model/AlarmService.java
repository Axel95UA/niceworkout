package com.axelua.niceworkout.model;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.axelua.niceworkout.WorkoutApp;
import com.axelua.niceworkout.config.AppPreferences;
import com.axelua.niceworkout.receivers.AlarmReceiver;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import static WorkoutDB.Workout.*;
import static android.text.format.DateUtils.*;

/**
 * created by Alex Ivanov on 08.08.15.
 */
public class AlarmService extends Service {

    @Inject
    protected AppPreferences prefs;

    private AlarmManager manager;
    private int notifications = 0;

    private PendingIntent absPendingIntent;
    private PendingIntent sitUpPendingIntent;

    @Override
    public void onCreate() {
        super.onCreate();
        WorkoutApp.getComponent().inject(this);
        if (manager == null) {
            manager = (AlarmManager) getSystemService(ALARM_SERVICE);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (prefs.isNotifyAbs()) {
            if (absPendingIntent == null) {
                Intent intentABS = new Intent(this, AlarmReceiver.class);
                intentABS.putExtra(WORKOUT_TYPE, ABS_WORKOUT);
                absPendingIntent = PendingIntent.getBroadcast(this, 0, intentABS, 0);
            }
            updateAbsNotificationTime();
            notifications++;
        }
        if (prefs.isNotifySitUp()) {
            if (sitUpPendingIntent == null) {
                Intent intentSitUp = new Intent(this, AlarmReceiver.class);
                intentSitUp.putExtra(WORKOUT_TYPE, SIT_UP_WORKOUT);
                sitUpPendingIntent = PendingIntent.getBroadcast(this, 0, intentSitUp, 0);
                updateSitUpNotificationTime();
                notifications++;
            }

        }
        checkStopService();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new AlarmBinder();
    }

    private long getNotificationTime(long time) {
        Calendar calendar = Calendar.getInstance();
        int hours = (int) TimeUnit.MILLISECONDS.toHours(time);
        int minutes = (int) (TimeUnit.MILLISECONDS.toMinutes(time) - hours * 60);
        calendar.set(Calendar.HOUR_OF_DAY, hours);
        calendar.set(Calendar.MINUTE, minutes);
        time = calendar.getTimeInMillis();
        if (System.currentTimeMillis() > time) {
            time += DAY_IN_MILLIS;
        }
        return time;
    }

    private void checkStopService() {
        if (notifications == 0) {
            stopSelf();
        }
    }

    public void stopAbsNotifications() {
        manager.cancel(absPendingIntent);
        notifications--;
        checkStopService();
    }

    public void stopSitUpNotifications() {
        manager.cancel(sitUpPendingIntent);
        notifications--;
        checkStopService();
    }

    public void updateAbsNotificationTime() {
        manager.setRepeating(AlarmManager.RTC_WAKEUP,
                getNotificationTime(prefs.getTimeOfAbsNotifications()),
                prefs.getAbsNotificationFrequency(), absPendingIntent);
    }

    public void updateSitUpNotificationTime() {
        manager.setRepeating(AlarmManager.RTC_WAKEUP,
                getNotificationTime(prefs.getTimeOfSitUpNotifications()),
                prefs.getSitUpNotificationFrequency(), sitUpPendingIntent);
    }

    public class AlarmBinder extends Binder {

        public AlarmService getAlarmService() {
            return AlarmService.this;
        }
    }
}
