package com.axelua.niceworkout.model;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.ui.TrainingActivity;

import java.util.Random;

import static WorkoutDB.Workout.ABS_WORKOUT;
import static WorkoutDB.Workout.SIT_UP_WORKOUT;
import static WorkoutDB.Workout.WORKOUT_TYPE;

/**
 * created by Alex Ivanov on 08.08.15.
 */
public class NotificationService extends IntentService {

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */
    public NotificationService() {
        super("NotificationService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        int type = intent.getIntExtra(TrainingActivity.EXTRA_TYPE, -1);
        Intent i = new Intent(getApplicationContext(), TrainingActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.putExtra(WORKOUT_TYPE, type);

        String workout = "";
        switch (type) {
            case ABS_WORKOUT:
                workout = getString(R.string.title_addition_abs);
                break;
            case SIT_UP_WORKOUT:
                workout = getString(R.string.title_addition_sit_up);
                break;
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
        stackBuilder.addParentStack(TrainingActivity.class);
        stackBuilder.addNextIntent(i);
        PendingIntent pendingIntent = stackBuilder.
                getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.notification_title, workout))
                .setContentText(getText(R.string.notification_text))
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .build();

        ((NotificationManager) getSystemService(NOTIFICATION_SERVICE)).
                notify(new Random().nextInt(), notification);
    }
}
