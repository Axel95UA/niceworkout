package com.axelua.niceworkout.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.events.DrawerItemClickEvent;

import de.greenrobot.event.EventBus;

/**
 * created by Alex Ivanov on 14.07.15.
 */
public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private LayoutInflater layoutInflater;

    public DrawerAdapter(Context c) {
        layoutInflater = LayoutInflater.from(c);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new ViewHolder(
                        layoutInflater.inflate(R.layout.header, parent, false),
                        viewType);
            case TYPE_ITEM:
                return new ViewHolder(
                        layoutInflater.inflate(R.layout.drawer_list_item, parent, false),
                        viewType);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (holder.holderId == 1) {
            DrawerItem item = holder.getItem(position);
            holder.titleTextView.setText(item.title);
            holder.iconImageView.setImageResource(item.icon);
        }//TODO header view
    }

    @Override
    public int getItemCount() {
        return DrawerItem.values().length + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        } else {
            return TYPE_ITEM;
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private int holderId;
        private TextView titleTextView;
        private ImageView iconImageView;

        public ViewHolder(View itemView, int ViewType) {
            super(itemView);

            if (ViewType == TYPE_ITEM) {
                titleTextView = (TextView) itemView.findViewById(R.id.drawer_list_item_title_textView);
                iconImageView = (ImageView) itemView.findViewById(R.id.drawer_item_icon_imageView);
                itemView.setOnClickListener(this);
                holderId = 1;
            } else {
                holderId = 0;
            }
        }

        @Override
        public void onClick(View v) {
            EventBus.getDefault().post(new DrawerItemClickEvent(getItem(getAdapterPosition())));
        }

        public DrawerItem getItem(int position) {
            return DrawerItem.values()[position - 1];
        }

    }

    public enum DrawerItem {
        ABS(R.string.drawer_abs, R.drawable.abs),
        SITUP(R.string.drawer_sit_up, R.drawable.situp);

        private int title;
        private int icon;

        DrawerItem(int title, int icon) {
            this.title = title;
            this.icon = icon;
        }

        public int getTitle() {
            return title;
        }
    }

}
