package com.axelua.niceworkout.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.WorkoutApp;
import com.axelua.niceworkout.config.AppDatabase;
import com.axelua.niceworkout.config.AppPreferences;
import com.axelua.niceworkout.utils.DateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import WorkoutDB.Workout;

/**
 * created by Alex Ivanov on 13.08.15.
 */
public class WorkoutStatisticsAdapter extends RecyclerView.Adapter<WorkoutStatisticsAdapter.StatsViewHolder> {

    @Inject
    protected AppPreferences prefs;
    @Inject
    protected AppDatabase database;

    private final int TYPE_SHORT_STAT = 0;
    private final int TYPE_SHOW_BUTTON = 1;
    private final int TYPE_LIST = 2;
    private LayoutInflater layoutInflater;
    private List<Workout> workouts;
    private List<Workout> workoutList;
    private int workoutType;
    private Context context;

    public WorkoutStatisticsAdapter(Context c, int workoutType) {
        layoutInflater = LayoutInflater.from(c);
        WorkoutApp.getComponent().inject(this);
        this.workoutType = workoutType;
        context = c;
        workouts = new ArrayList<>();
        new LoadData().execute();
    }

    @Override
    public StatsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int res;
        switch (viewType) {
            case TYPE_SHORT_STAT:
                res = R.layout.short_workout_statistics;
                break;
            case TYPE_SHOW_BUTTON:
                res = R.layout.show_workouts_button;
                break;
            case TYPE_LIST:
                res = R.layout.workout_item;
                break;
            default:
                return null;
        }
        return new StatsViewHolder(layoutInflater.inflate(res, parent, false), viewType);
    }

    @Override
    public void onBindViewHolder(StatsViewHolder holder, int position) {
        switch (holder.holderID) {
            case TYPE_SHORT_STAT:
                updateShortStatistics(holder);
                break;
            case TYPE_SHOW_BUTTON:
                if (prefs.getAbsWorkoutsCount() == 0) {
                    holder.showWorkoutsButton.setVisibility(View.GONE);
                } else if (workouts.size() == 0) {
                    holder.showWorkoutsButton.setText(R.string.show_all_workouts);
                } else {
                    holder.showWorkoutsButton.setText(R.string.hide_workouts);
                }
                break;
            case TYPE_LIST:
                Workout workout = workouts.get(position - 2);
                holder.countTextView.setText(context.getString(R.string.workout_count, workout.getCount()));
                holder.speedTextView.setText(context.getString(R.string.workout_speed, String.format("%.2f", workout.getSpeed())));
                Date workoutDate = new Date(workout.getTime());
                holder.dateTextView.setText(context.getString(R.string.workout_date,
                        DateUtils.getDate((workoutDate)), DateUtils.getTime(workoutDate)));
                break;
        }
    }

    public void addWorkout(Workout workout) {
        notifyItemChanged(0);
        if (workouts.size() == 0) {
            workoutList.add(0, workout);
        } else {
            notifyItemInserted(2);
            workouts.add(0, workout);
        }
    }

    private void updateShortStatistics(StatsViewHolder holder) {
        switch (workoutType) {
            case Workout.ABS_WORKOUT:
                holder.meanCountTextView.setText(context.getString(R.string.mean_count,
                        String.format("%.2f", prefs.getMeanAbsCount())));
                holder.bestCountTextView.setText(context.getString(R.string.best_count,
                        prefs.getBestAbsCount()));
                holder.meanSpeedTextView.setText(context.getString(R.string.mean_speed,
                        String.format("%.2f", prefs.getMeanAbsSpeed())));
                holder.bestSpeedTextView.setText(context.getString(R.string.best_speed,
                        String.format("%.2f", prefs.getBestAbsSpeed())));
                break;
            case Workout.SIT_UP_WORKOUT:
                holder.meanCountTextView.setText(context.getString(R.string.mean_count,
                        String.format("%.2f", prefs.getMeanSitUpCount())));
                holder.bestCountTextView.setText(context.getString(R.string.best_count,
                        prefs.getBestSitUpCount()));
                holder.meanSpeedTextView.setText(context.getString(R.string.mean_speed,
                        String.format("%.2f", prefs.getMeanSitUpSpeed())));
                holder.bestSpeedTextView.setText(context.getString(R.string.best_speed,
                        String.format("%.2f", prefs.getBestSitUpSpeed())));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_SHORT_STAT;
            case 1:
                return TYPE_SHOW_BUTTON;
            default:
                return TYPE_LIST;
        }
    }

    @Override
    public int getItemCount() {
        return workouts.size() + 2;
    }

    protected class StatsViewHolder extends RecyclerView.ViewHolder {

        private int holderID;
        //Short stats item
        private TextView meanCountTextView;
        private TextView bestCountTextView;
        private TextView meanSpeedTextView;
        private TextView bestSpeedTextView;
        //show button
        private Button showWorkoutsButton;
        //list items
        private TextView countTextView;
        private TextView speedTextView;
        private TextView dateTextView;


        public StatsViewHolder(View itemView, int type) {
            super(itemView);

            switch (type) {
                case TYPE_SHORT_STAT:
                    meanCountTextView = (TextView) itemView.findViewById(R.id.statistics_average_count_textView);
                    bestCountTextView = (TextView) itemView.findViewById(R.id.statistics_best_count_textView);
                    meanSpeedTextView = (TextView) itemView.findViewById(R.id.statistics_average_time_textView);
                    bestSpeedTextView = (TextView) itemView.findViewById(R.id.statistics_best_time_textView);
                    break;
                case TYPE_SHOW_BUTTON:
                    showWorkoutsButton = (Button) itemView.findViewById(R.id.show_workouts_button);
                    showWorkoutsButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (workouts.size() == 0) {
                                notifyItemChanged(1);
                                notifyItemRangeInserted(2, workoutList.size());
                                workouts = workoutList;
                                workoutList = new ArrayList<>();
                            } else {
                                notifyItemChanged(1);
                                notifyItemRangeRemoved(2, workouts.size());
                                workoutList = workouts;
                                workouts = new ArrayList<>();
                            }
                        }
                    });
                    break;
                case TYPE_LIST:
                    countTextView = (TextView) itemView.findViewById(R.id.workout_item_count_textView);
                    speedTextView = (TextView) itemView.findViewById(R.id.workout_item_speed_textView);
                    dateTextView = (TextView) itemView.findViewById(R.id.workout_item_date_textView);
                    break;
            }
            holderID = type;
        }
    }

    private class LoadData extends AsyncTask<Void, Void, List<Workout>> {

        @Override
        protected List<Workout> doInBackground(Void... params) {
            return database.getAllWorkoutByType(workoutType);
        }

        @Override
        protected void onPostExecute(List<Workout> workoutsDB) {
            super.onPostExecute(workoutsDB);
            workoutList = workoutsDB;
        }
    }
}