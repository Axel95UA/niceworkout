package com.axelua.niceworkout.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.axelua.niceworkout.ui.Workouts.AbsSettingsFragment;
import com.axelua.niceworkout.ui.Workouts.SitUpSettingsFragment;
import com.axelua.niceworkout.ui.Workouts.TabCalendarFragment;
import com.axelua.niceworkout.ui.Workouts.TabStatisticsFragment;
import com.axelua.niceworkout.ui.Workouts.TabTrainingFragment;

import WorkoutDB.Workout;

/**
 * created by Alex Ivanov on 16.07.15.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    private final String[] titles;
    private final int adapterType;

    public ViewPagerAdapter(FragmentManager fm, String[] titles, int adapterType) {
        super(fm);
        this.titles = titles;
        this.adapterType = adapterType;
    }

    @Override
    public Fragment getItem(int position) {
        Bundle bundle = new Bundle();
        bundle.putInt(Workout.WORKOUT_TYPE, adapterType);
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new TabTrainingFragment();
                break;
            case 1:
                fragment = new TabStatisticsFragment();
                break;
            case 2:
                fragment = new TabCalendarFragment();
                break;
            case 3:
                if (adapterType == Workout.ABS_WORKOUT) {
                    fragment = new AbsSettingsFragment();
                } else {
                    fragment = new SitUpSettingsFragment();
                }
                break;
            default:
                fragment = new TabTrainingFragment();
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}
