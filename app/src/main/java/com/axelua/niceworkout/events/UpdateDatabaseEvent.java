package com.axelua.niceworkout.events;

import WorkoutDB.Workout;

/**
 * created by Alex Ivanov on 06.08.15.
 */
public class UpdateDatabaseEvent {

    private final Workout workout;
    private int count;

    public UpdateDatabaseEvent(Workout workout) {
        this.workout = workout;
        this.count = 2;
    }

    public Workout getWorkout() {
        return workout;
    }

    public int getCount() {
        return --count;
    }
}
