package com.axelua.niceworkout.events;

import com.axelua.niceworkout.adapters.DrawerAdapter.DrawerItem;

/**
 * created by Alex Ivanov on 15.07.15.
 */
public class DrawerItemClickEvent {

    private final DrawerItem item;

    public DrawerItemClickEvent(DrawerItem item) {
        this.item = item;
    }

    public DrawerItem getItem() {
        return item;
    }
}
