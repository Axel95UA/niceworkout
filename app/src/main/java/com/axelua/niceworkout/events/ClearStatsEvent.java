package com.axelua.niceworkout.events;

/**
 * created by Alex Ivanov on 11.08.15.
 */
public class ClearStatsEvent {

    private int count;

    public ClearStatsEvent(){
        this.count = 1;
    }

    public int getCount() {
        return  --count;
    }
}
