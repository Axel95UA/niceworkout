package com.axelua.niceworkout.ui;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.axelua.niceworkout.WorkoutApp;
import com.axelua.niceworkout.config.AppDatabase;
import com.axelua.niceworkout.config.AppPreferences;
import com.axelua.niceworkout.model.AlarmService;

import javax.inject.Inject;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * created by Alex Ivanov on 13.07.15.
 */
public abstract class BaseFragment extends Fragment {

    protected final String tag = getClass().getSimpleName();

    @Inject
    protected AppDatabase database;
    @Inject
    protected AppPreferences prefs;
    protected EventBus BUS;

    protected abstract int getContentView();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WorkoutApp.getComponent().inject(this);
        BUS = EventBus.getDefault();
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(getContentView(), container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
