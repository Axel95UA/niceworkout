package com.axelua.niceworkout.ui;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.adapters.DrawerAdapter;
import com.axelua.niceworkout.adapters.DrawerAdapter.DrawerItem;
import com.axelua.niceworkout.events.DrawerItemClickEvent;
import com.axelua.niceworkout.ui.Workouts.AbsWorkoutFragment;
import com.axelua.niceworkout.ui.Workouts.SitUpWorkoutFragment;

import butterknife.Bind;

import static com.axelua.niceworkout.adapters.DrawerAdapter.DrawerItem.*;

/**
 * created by Alex Ivanov on 14.07.15.
 */
public class MainActivity extends BaseActivity {

    @Bind(R.id.drawer_layout)
    protected DrawerLayout drawerLayout;
    @Bind(R.id.drawer_contained_layout)
    protected RecyclerView drawerRecyclerView;
    @Bind(R.id.main_toolbar)
    protected Toolbar mainToolbar;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    protected int getContentView() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(mainToolbar);

        drawerRecyclerView.hasFixedSize();
        drawerRecyclerView.setAdapter(new DrawerAdapter(this));
        drawerRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        drawerRecyclerView.setItemAnimator(new DefaultItemAnimator());

        setupDrawer();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        BUS.register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        BUS.unregister(this);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
        startFragment(ABS);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return drawerToggle.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this,
                drawerLayout,
                R.string.drawer_open,
                R.string.drawer_close) {

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.setDrawerListener(drawerToggle);
    }


    private void startFragment(DrawerItem item) {
        switch (item) {
            case ABS:
                runFragment(new AbsWorkoutFragment());
                break;
            case SITUP:
                runFragment(new SitUpWorkoutFragment());
                break;
            default:
                Log.w(tag, "unknown drawer item");
        }
        mainToolbar.setTitle(item.getTitle());
        drawerLayout.closeDrawer(drawerRecyclerView);
    }

    private void runFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.main_container, fragment).commit();
    }

    @SuppressWarnings("unused")
    public void onEvent(DrawerItemClickEvent event) {
        startFragment(event.getItem());
    }
}
