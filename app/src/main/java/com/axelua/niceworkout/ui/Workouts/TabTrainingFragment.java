package com.axelua.niceworkout.ui.Workouts;

import android.content.Intent;
import android.os.Bundle;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.adapters.ViewPagerAdapter;
import com.axelua.niceworkout.ui.BaseFragment;
import com.axelua.niceworkout.ui.TrainingActivity;

import WorkoutDB.Workout;
import butterknife.OnClick;

/**
 * created by Alex Ivanov on 17.07.15.
 */
public class TabTrainingFragment extends BaseFragment {

    private int type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = getArguments().getInt(Workout.WORKOUT_TYPE);
    }

    @Override
    protected int getContentView() {
        return R.layout.fragment_tab_training;
    }

    @OnClick(R.id.training_button)
    public void startTraining(){
        Intent i = new Intent(getActivity(), TrainingActivity.class);
        i.putExtra(TrainingActivity.EXTRA_TYPE, type);
        startActivity(i);
    }
}
