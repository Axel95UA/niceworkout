package com.axelua.niceworkout.ui.Workouts;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.TextView;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.events.ClearStatsEvent;
import com.axelua.niceworkout.events.UpdateDatabaseEvent;
import com.axelua.niceworkout.ui.BaseFragment;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import WorkoutDB.Workout;
import butterknife.Bind;

/**
 * created by Alex Ivanov on 01.08.15.
 */
public class TabCalendarFragment extends BaseFragment {

    @Bind(R.id.calendar_count_textView)
    protected TextView countTextView;

    private int type;
    private CaldroidFragment caldroidFragment;

    @Override
    protected int getContentView() {
        return R.layout.fragment_calendar_tab;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        type = getArguments().getInt(Workout.WORKOUT_TYPE);
        createCalendarFragment();
    }

    @Override
    public void onStart() {
        super.onStart();
        BUS.registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BUS.unregister(this);
    }

    private void drawDateBackground(long time) {
        Date selectedDate = new Date(time);
        if (database.getByDate(selectedDate, type).size() != 0) {
            if (DateUtils.isToday(time)) {
                caldroidFragment.setBackgroundResourceForDate
                        (R.drawable.calendar_today_training, selectedDate);
            } else {
                caldroidFragment.setBackgroundResourceForDate
                        (R.color.primary_color, selectedDate);
            }
        } else {
            caldroidFragment.setBackgroundResourceForDate(R.color.caldroid_white, selectedDate);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(UpdateDatabaseEvent updateDatabaseEvent) {
        drawDateBackground(updateDatabaseEvent.getWorkout().getTime());
        caldroidFragment.refreshView();
        if (updateDatabaseEvent.getCount() == 0) {
            BUS.removeStickyEvent(updateDatabaseEvent);
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(ClearStatsEvent clearStatsEvent) {
        createCalendarFragment();
        if (clearStatsEvent.getCount() == 0) {
            BUS.removeStickyEvent(clearStatsEvent);
        }
    }

    private void createCalendarFragment(){
        caldroidFragment = new CaldroidFragment();
        Bundle args = new Bundle();
        Calendar calendar = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, calendar.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, calendar.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, false);
        caldroidFragment.setArguments(args);
        getActivity().getSupportFragmentManager().beginTransaction().
                replace(R.id.calendar_frameLayout, caldroidFragment).commit();
        caldroidFragment.setCaldroidListener(new CalendarListener());
        new InitWorkouts().execute();
    }

    private class CalendarListener extends CaldroidListener {

        private Date previousSelection = null;

        @Override
        public void onSelectDate(Date date, View view) {
            if (previousSelection != null) {
                drawDateBackground(previousSelection.getTime());
                caldroidFragment.refreshView();
            }
            caldroidFragment.setBackgroundResourceForDate(R.color.caldroid_sky_blue, date);
            caldroidFragment.refreshView();
            new SelectDateTask(date).execute();
            previousSelection = date;
        }
    }

    private class InitWorkouts extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            List<Workout> workouts = database.getAllWorkoutByType(type);
            for (Workout training : workouts) {
                drawDateBackground(training.getTime());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            caldroidFragment.refreshView();
        }
    }

    private class SelectDateTask extends AsyncTask<Void, Void, List<Workout>> {

        private final Date date;

        public SelectDateTask(Date date) {
            this.date = date;
        }

        @Override
        protected List<Workout> doInBackground(Void... params) {
            return database.getByDate(date, type);
        }

        @Override
        protected void onPostExecute(List<Workout> workouts) {
            super.onPostExecute(workouts);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < workouts.size(); i++) {
                sb.append(workouts.get(i).getCount());
                if (i + 1 < workouts.size() && i < 2) {
                    sb.append(", ");
                }
                if (i > 1) {
                    break;
                }
            }
            countTextView.setText(sb.toString());
        }
    }
}