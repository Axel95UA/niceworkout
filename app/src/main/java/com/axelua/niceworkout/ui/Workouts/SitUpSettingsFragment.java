package com.axelua.niceworkout.ui.Workouts;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.SwitchPreference;
import android.support.v7.app.AlertDialog;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.events.ClearStatsEvent;
import com.axelua.niceworkout.model.AlarmService;
import com.axelua.niceworkout.ui.BaseSettingsFragment;
import com.axelua.niceworkout.utils.DateUtils;

import java.util.concurrent.TimeUnit;

import WorkoutDB.Workout;

/**
 * created by Alex Ivanov on 17.08.15.
 */
public class SitUpSettingsFragment extends BaseSettingsFragment {

    private Preference timeNotificationPreference;

    @Override
    protected int getXmlResource() {
        return R.xml.sit_up_settings_fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SwitchPreference enableNotificationsSwitchPreference = (SwitchPreference) findPreference("notifySitUpWorkout");
        enableNotificationsSwitchPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                if (newValue.equals(Boolean.FALSE)) {
                    service.stopSitUpNotifications();
                } else {
                    getActivity().startService(new Intent(getActivity(), AlarmService.class));
                    getActivity().bindService(new Intent(getActivity(), AlarmService.class),
                            serviceConnection, 0);
                }
                return true;
            }
        });

        timeNotificationPreference = findPreference("notifySitUpTime");
        updateTimeNotificationSummary();
        timeNotificationPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                long time = prefs.getTimeOfSitUpNotifications();
                int hour = (int) TimeUnit.MILLISECONDS.toHours(time);
                int minute = (int) TimeUnit.MILLISECONDS.toMinutes(time) - hour * 60;
                TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                        new TimePickerSetListener(), hour, minute, true);
                timePickerDialog.show();
                return false;
            }
        });

        final ListPreference frequencyListPreference = (ListPreference) findPreference("workoutSitUpNotificationFrequency");
        frequencyListPreference.setSummary(frequencyListPreference.getEntry());
        frequencyListPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                frequencyListPreference.setValue(newValue.toString());
                frequencyListPreference.setSummary(frequencyListPreference.getEntry());
                if (service != null && prefs.isNotifySitUp()) {
                    service.updateSitUpNotificationTime();
                }
                return false;
            }
        });

        Preference clearStatisticsPreference = findPreference("clearSitUpStats");
        clearStatisticsPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(R.string.clear_stats_dialog);
                builder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        prefs.clearSitUpPrefs();
                        database.clearStatsByType(Workout.SIT_UP_WORKOUT);
                        BUS.postSticky(new ClearStatsEvent());
                    }
                }).setNegativeButton(R.string.no, null).show();
                return false;
            }
        });
    }

    @Override
    protected void updateTimeNotificationSummary() {
        timeNotificationPreference.setSummary(DateUtils.getTimeForSettings(prefs.getTimeOfSitUpNotifications()));

    }
}
