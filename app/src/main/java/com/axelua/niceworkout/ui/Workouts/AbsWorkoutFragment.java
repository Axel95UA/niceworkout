package com.axelua.niceworkout.ui.Workouts;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.adapters.ViewPagerAdapter;
import com.axelua.niceworkout.tabs.SlidingTabLayout;
import com.axelua.niceworkout.ui.BaseFragment;

import butterknife.Bind;

import static WorkoutDB.Workout.ABS_WORKOUT;

/**
 * created by Alex Ivanov on 17.07.15.
 */
public class AbsWorkoutFragment extends BaseFragment {

    @Bind(R.id.tabs)
    protected SlidingTabLayout tabLayout;
    @Bind(R.id.main_pager)
    protected ViewPager viewPager;

    @Override
    protected int getContentView() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager.setAdapter(new ViewPagerAdapter(
                getActivity().getSupportFragmentManager(),
                getResources().getStringArray(R.array.tab_items),
                ABS_WORKOUT));
        tabLayout.setDistributeEvenly(true);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabs_scroll_color);
            }
        });
        tabLayout.setViewPager(viewPager);
    }
}
