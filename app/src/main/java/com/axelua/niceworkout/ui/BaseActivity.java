package com.axelua.niceworkout.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.axelua.niceworkout.WorkoutApp;
import com.axelua.niceworkout.config.AppDatabase;
import com.axelua.niceworkout.config.AppPreferences;

import javax.inject.Inject;

import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;

/**
 * created by Alex Ivanov on 13.07.15.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected final String tag = getClass().getSimpleName();
    @Inject
    protected AppPreferences prefs;
    @Inject
    protected AppDatabase database;
    protected EventBus BUS;

    protected abstract int getContentView();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getContentView());
        WorkoutApp.getComponent().inject(this);
        ButterKnife.bind(this);
        BUS = EventBus.getDefault();
    }

    protected void makeToast(int stringResource) {
        Toast.makeText(this, stringResource, Toast.LENGTH_SHORT).show();
    }
}
