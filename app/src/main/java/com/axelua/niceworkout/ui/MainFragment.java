package com.axelua.niceworkout.ui;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.adapters.ViewPagerAdapter;
import com.axelua.niceworkout.tabs.SlidingTabLayout;

import WorkoutDB.Workout;
import butterknife.Bind;

/**
 * created by Alex Ivanov on 16.07.15.
 */
public class MainFragment extends BaseFragment {

    @Bind(R.id.main_pager)
    protected ViewPager viewPager;
    @Bind(R.id.tabs)
    protected SlidingTabLayout tabLayout;

    @Override
    protected int getContentView() {
        return R.layout.fragment_main;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewPager.setAdapter(new ViewPagerAdapter(
                getActivity().getSupportFragmentManager(), new String[1], Workout.ABS_WORKOUT));
        tabLayout.setDistributeEvenly(true);
        tabLayout.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabs_scroll_color);
            }
        });
        tabLayout.setViewPager(viewPager);
    }
}
