package com.axelua.niceworkout.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.widget.TextView;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.events.UpdateDatabaseEvent;

import WorkoutDB.Workout;
import butterknife.Bind;
import butterknife.OnClick;

import static WorkoutDB.Workout.ABS_WORKOUT;
import static android.view.KeyEvent.*;

/**
 * created by Alex Ivanov on 18.07.15.
 */
public class TrainingActivity extends BaseActivity {

    public static final String EXTRA_TYPE = "workout_type";

    @Bind(R.id.training_count_textView)
    protected TextView trainingCountTextView;

    private int count = 0;
    private int type;
    private long startTime;
    private long endTime;

    @Override
    protected int getContentView() {
        return R.layout.activity_training;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        trainingCountTextView.setText(String.valueOf(count));
        type = getIntent().getIntExtra(EXTRA_TYPE, -1);
    }

    @Override
    public boolean dispatchKeyEvent(@NonNull KeyEvent event) {
        if (type == ABS_WORKOUT) {
            switch (event.getKeyCode()) {
                case KEYCODE_VOLUME_UP:
                    if (event.getAction() == ACTION_UP) {
                        updateCounter();
                    }
                    return true;
                case KEYCODE_VOLUME_DOWN:
                    if (event.getAction() == ACTION_UP) {
                        updateCounter();
                    }
                    return true;
                default:
                    return super.dispatchKeyEvent(event);
            }
        } else {
            return super.dispatchKeyEvent(event);
        }
    }

    @OnClick(R.id.plus_one_button)
    protected void updateCounter() {
        if (count == 0) {
            startTime = System.currentTimeMillis();
        } else {
            endTime = System.currentTimeMillis();
        }
        trainingCountTextView.setText(String.valueOf(++count));
    }

    @OnClick(R.id.end_training_button)
    public void endTraining() {
        if (count != 0) {
            long lastID = prefs.getAbsWorkoutsCount();
            lastID += prefs.getSitUpWorkoutCount();
            float speed = (float) (count - 1) / (endTime - startTime) * 1000;
            Workout item = new Workout(++lastID, type, endTime, count, speed);
            database.insertWorkout(item);
            switch (type) {
                case Workout.ABS_WORKOUT:
                    prefs.updateAbsCount(count);
                    prefs.updateAbsSpeed(speed);
                    prefs.updateAbsCount();
                    break;
                case Workout.SIT_UP_WORKOUT:
                    prefs.updateSitUpCount(count);
                    prefs.updateSitUpSpeed(speed);
                    prefs.updateSitUpWorkoutCount();
                    break;
            }
            BUS.postSticky(new UpdateDatabaseEvent(item));
        }
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.end_training).
                setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        endTraining();
                    }
                }).setNegativeButton(R.string.no, null)
                .show();
    }
}
