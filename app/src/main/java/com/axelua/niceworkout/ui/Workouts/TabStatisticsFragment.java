package com.axelua.niceworkout.ui.Workouts;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;

import com.axelua.niceworkout.R;
import com.axelua.niceworkout.adapters.WorkoutStatisticsAdapter;
import com.axelua.niceworkout.events.UpdateDatabaseEvent;
import com.axelua.niceworkout.ui.BaseFragment;

import WorkoutDB.Workout;
import butterknife.Bind;

/**
 * created by Alex Ivanov on 04.08.15.
 */
public class TabStatisticsFragment extends BaseFragment {

    @Bind(R.id.statistics_all_training_list)
    protected RecyclerView trainingList;

    private WorkoutStatisticsAdapter adapter;

    @Override
    protected int getContentView() {
        return R.layout.fragment_tab_statistics;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        int type = getArguments().getInt(Workout.WORKOUT_TYPE);
        adapter = new WorkoutStatisticsAdapter(getActivity(), type);
        trainingList.setItemAnimator(new DefaultItemAnimator());
        trainingList.setLayoutManager(new LinearLayoutManager(getActivity()));
        trainingList.setAdapter(adapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        BUS.registerSticky(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BUS.unregister(this);
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(UpdateDatabaseEvent updateDatabaseEvent) {
        adapter.addWorkout(updateDatabaseEvent.getWorkout());
        if (updateDatabaseEvent.getCount() == 0) {
            BUS.removeStickyEvent(updateDatabaseEvent);
        }
    }
}
