package com.axelua.niceworkout.ui;

import android.app.TimePickerDialog;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.TimePicker;

import com.axelua.niceworkout.WorkoutApp;
import com.axelua.niceworkout.config.AppDatabase;
import com.axelua.niceworkout.config.AppPreferences;
import com.axelua.niceworkout.model.AlarmService;
import com.github.machinarius.preferencefragment.PreferenceFragment;

import javax.inject.Inject;

import WorkoutDB.Workout;
import de.greenrobot.event.EventBus;

import static android.text.format.DateUtils.*;

/**
 * created by Alex Ivanov on 11.08.15.
 */
public abstract class BaseSettingsFragment extends PreferenceFragment {

    @Inject
    protected AppPreferences prefs;
    @Inject
    protected AppDatabase database;

    protected final String tag = getClass().getName();

    protected AlarmService service;
    protected ServiceConnection serviceConnection;
    protected boolean bound = false;
    protected EventBus BUS;

    protected abstract int getXmlResource();

    protected abstract void updateTimeNotificationSummary();

    private int type;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BUS = EventBus.getDefault();
        type = getArguments().getInt(Workout.WORKOUT_TYPE);
        WorkoutApp.getComponent().inject(this);
        addPreferencesFromResource(getXmlResource());

        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder binder) {
                Log.d(tag, "Service connected");
                service = ((AlarmService.AlarmBinder) binder).getAlarmService();
                bound = true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(tag, "Service disconnected");
                bound = false;
            }
        };
    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(new Intent(getActivity(), AlarmService.class),
                serviceConnection, 0);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (bound) {
            getActivity().unbindService(serviceConnection);
            bound = false;
        }
    }

    public class TimePickerSetListener implements TimePickerDialog.OnTimeSetListener {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Log.d(tag, String.format("Time is set for %02d:%02d", hourOfDay, minute));
            if (service != null) {
                switch (type) {
                    case Workout.ABS_WORKOUT:
                        prefs.setABSNotifyTime(HOUR_IN_MILLIS * hourOfDay + MINUTE_IN_MILLIS * minute);
                        if (prefs.isNotifyAbs()) {
                            service.updateAbsNotificationTime();
                        }
                        break;
                    case Workout.SIT_UP_WORKOUT:
                        prefs.setSitUpNotifyTime(HOUR_IN_MILLIS * hourOfDay + MINUTE_IN_MILLIS * minute);
                        if (prefs.isNotifySitUp()) {
                            service.updateSitUpNotificationTime();
                        }
                }
            }
            updateTimeNotificationSummary();
        }
    }

}
