package com.axelua.niceworkout.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.axelua.niceworkout.model.NotificationService;

import static WorkoutDB.Workout.WORKOUT_TYPE;

/**
 * created by Alex Ivanov on 08.08.15.
 */
public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent sendIntent = new Intent(context, NotificationService.class);
        sendIntent.putExtra(WORKOUT_TYPE, intent.getIntExtra(WORKOUT_TYPE, -1));
        context.startService(sendIntent);
    }
}
