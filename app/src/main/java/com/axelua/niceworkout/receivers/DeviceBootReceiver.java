package com.axelua.niceworkout.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.axelua.niceworkout.model.AlarmService;

/**
 * created by Alex Ivanov on 08.08.15.
 */
public class DeviceBootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            context.startService(new Intent(context, AlarmService.class));
        }
    }
}
