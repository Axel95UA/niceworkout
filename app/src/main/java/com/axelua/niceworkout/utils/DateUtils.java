package com.axelua.niceworkout.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static android.text.format.DateUtils.HOUR_IN_MILLIS;

/**
 * created by Alex Ivanov on 06.08.15.
 */
public class DateUtils {

    private static final ThreadLocal<DateFormat> SDF_DATE = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd MMMM", Locale.getDefault());
        }
    };
    private static final ThreadLocal<DateFormat> SDF_TIME = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("HH:mm", Locale.getDefault());
        }
    };

    private DateUtils() {
    }

    public static String getDate(Date time) {
        return SDF_DATE.get().format(time);
    }

    public static String getTime(Date time) {
        return SDF_TIME.get().format(time);
    }

    public static long getEndOfDay(Date time) {
        return time.getTime() + TimeUnit.DAYS.toMillis(1);
    }

    public static String getTimeForSettings(long time) {
        long hours = TimeUnit.MILLISECONDS.toHours(time);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(time) - hours * 60;
        return String.format("%02d:%02d", hours, minutes);
    }
}
