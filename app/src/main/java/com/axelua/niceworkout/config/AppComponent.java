package com.axelua.niceworkout.config;

import com.axelua.niceworkout.adapters.WorkoutStatisticsAdapter;
import com.axelua.niceworkout.model.AlarmService;
import com.axelua.niceworkout.ui.BaseActivity;
import com.axelua.niceworkout.ui.BaseFragment;
import com.axelua.niceworkout.ui.BaseSettingsFragment;
import com.axelua.niceworkout.ui.Workouts.AbsSettingsFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * created by Alex Ivanov on 13.07.15.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

    void inject(AlarmService alarmService);

    void inject(BaseSettingsFragment settingsFragment);

    void inject(WorkoutStatisticsAdapter adapter);

}
