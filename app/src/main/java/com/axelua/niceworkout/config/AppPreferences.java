package com.axelua.niceworkout.config;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * created by Alex Ivanov on 13.07.15.
 */
public class AppPreferences {

    private static final String TAG_ABS_COUNT = "lastAbsID";
    private static final String TAG_MEAN_ABS_COUNT = "meanAbsCount";
    private static final String TAG_BEST_ABS_COUNT = "bestAbsCount";
    private static final String TAG_MEAN_ABS_SPEED = "meanAbsSpeed";
    private static final String TAG_BEST_ABS_SPEED = "bestAbsSpeed";

    private static final String TAG_SIT_UP_COUNT = "lastSitUpID";
    private static final String TAG_MEAN_SIT_UP_COUNT = "meanSitUpCount";
    private static final String TAG_BEST_SIT_UP_COUNT = "bestSitUpCount";
    private static final String TAG_MEAN_SIT_UP_SPEED = "meanSitUpSpeed";
    private static final String TAG_BEST_SIT_UP_SPEED = "bestSitUpSpeed";

    private final SharedPreferences prefs;

    public AppPreferences(Context context) {
        this.prefs = PreferenceManager.getDefaultSharedPreferences(context);

    }

    //ABS WORKOUT PREFERENCES

    public void updateAbsCount() {
        prefs.edit().putInt(TAG_ABS_COUNT, getAbsWorkoutsCount() + 1).apply();
    }

    public int getAbsWorkoutsCount() {
        return prefs.getInt(TAG_ABS_COUNT, 0);
    }

    public void updateAbsCount(int lastCount) {
        long absWorkoutsCount = getAbsWorkoutsCount();
        prefs.edit().putFloat(TAG_MEAN_ABS_COUNT,
                (getMeanAbsCount() * absWorkoutsCount + lastCount) / (absWorkoutsCount + 1)).apply();
        if (lastCount > getBestAbsCount()) {
            prefs.edit().putInt(TAG_BEST_ABS_COUNT, lastCount).apply();
        }
    }

    public float getMeanAbsCount() {
        return prefs.getFloat(TAG_MEAN_ABS_COUNT, 0);
    }

    public int getBestAbsCount() {
        return prefs.getInt(TAG_BEST_ABS_COUNT, 0);
    }

    public void updateAbsSpeed(float lastSpeed) {
        long lastID = getAbsWorkoutsCount();
        prefs.edit().putFloat(TAG_MEAN_ABS_SPEED,
                (getMeanAbsSpeed() * lastID + lastSpeed) / (lastID + 1)).apply();
        if (lastSpeed > getBestAbsSpeed()) {
            prefs.edit().putFloat(TAG_BEST_ABS_SPEED, lastSpeed).apply();
        }
    }

    public float getMeanAbsSpeed() {
        return prefs.getFloat(TAG_MEAN_ABS_SPEED, 0);
    }

    public float getBestAbsSpeed() {
        return prefs.getFloat(TAG_BEST_ABS_SPEED, 0);
    }

    public boolean isNotifyAbs() {
        return prefs.getBoolean("notifyABSWorkout", true);
    }

    public long getTimeOfAbsNotifications() {
        return prefs.getLong("notifyAbsTime", 68400000l);
    }

    public void setABSNotifyTime(long time) {
        prefs.edit().putLong("notifyAbsTime", time).apply();
    }

    public long getAbsNotificationFrequency() {
        return Long.valueOf(prefs.getString("workoutAbsNotificationFrequency", "86400000"));
    }

    public void clearAbsPrefs() {
        prefs.edit().putInt(TAG_ABS_COUNT, 0).apply();
        prefs.edit().putFloat(TAG_MEAN_ABS_COUNT, 0).apply();
        prefs.edit().putInt(TAG_BEST_ABS_COUNT, 0).apply();
        prefs.edit().putFloat(TAG_MEAN_ABS_SPEED, 0).apply();
        prefs.edit().putFloat(TAG_BEST_ABS_SPEED, 0).apply();
    }

    //SIT UP WORKOUT PREFERENCES

    public void updateSitUpWorkoutCount() {
        prefs.edit().putInt(TAG_SIT_UP_COUNT, getSitUpWorkoutCount()).apply();
    }

    public int getSitUpWorkoutCount() {
        return prefs.getInt(TAG_SIT_UP_COUNT, 0);
    }

    public void updateSitUpCount(int lastCount) {
        long sitUpWorkoutsCount = getSitUpWorkoutCount();
        prefs.edit().putFloat(TAG_MEAN_SIT_UP_COUNT,
                (getMeanSitUpCount() * sitUpWorkoutsCount + lastCount) / (sitUpWorkoutsCount + 1)).apply();
        if (lastCount > getBestSitUpCount()) {
            prefs.edit().putInt(TAG_BEST_SIT_UP_COUNT, lastCount).apply();
        }
    }

    public float getMeanSitUpCount() {
        return prefs.getFloat(TAG_MEAN_SIT_UP_COUNT, 0);
    }

    public int getBestSitUpCount() {
        return prefs.getInt(TAG_BEST_SIT_UP_COUNT, 0);
    }

    public void updateSitUpSpeed(float lastSpeed) {
        long sitUpWorkoutsCount = getSitUpWorkoutCount();
        prefs.edit().putFloat(TAG_MEAN_SIT_UP_SPEED,
                (getMeanSitUpSpeed() * sitUpWorkoutsCount + lastSpeed) / (sitUpWorkoutsCount + 1)).apply();
        if (lastSpeed > getBestSitUpSpeed()) {
            prefs.edit().putFloat(TAG_BEST_SIT_UP_SPEED, lastSpeed);
        }
    }

    public float getMeanSitUpSpeed() {
        return prefs.getFloat(TAG_MEAN_SIT_UP_SPEED, 0);
    }

    public float getBestSitUpSpeed() {
        return prefs.getFloat(TAG_BEST_SIT_UP_SPEED, 0);
    }

    public boolean isNotifySitUp() {
        return prefs.getBoolean("notifySitUpWorkout", true);
    }

    public long getTimeOfSitUpNotifications() {
        return prefs.getLong("notifySitUpTime", 68400000l);
    }

    public void setSitUpNotifyTime(long time) {
        prefs.edit().putLong("notifySitUpTime", time).apply();
    }

    public long getSitUpNotificationFrequency() {
        return Long.valueOf(prefs.getString("workoutSitUpNotificationFrequency", "86400000"));
    }

    public void clearSitUpPrefs() {
        prefs.edit().putInt(TAG_SIT_UP_COUNT, 0).apply();
        prefs.edit().putFloat(TAG_MEAN_SIT_UP_COUNT, 0).apply();
        prefs.edit().putInt(TAG_BEST_SIT_UP_COUNT, 0).apply();
        prefs.edit().putFloat(TAG_MEAN_SIT_UP_SPEED, 0).apply();
        prefs.edit().putFloat(TAG_BEST_SIT_UP_SPEED, 0).apply();
    }
}
