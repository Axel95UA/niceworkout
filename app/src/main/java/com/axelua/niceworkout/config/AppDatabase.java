package com.axelua.niceworkout.config;

import android.content.Context;

import com.axelua.niceworkout.utils.DateUtils;

import java.util.Date;
import java.util.List;

import WorkoutDB.DaoMaster;
import WorkoutDB.Workout;
import WorkoutDB.WorkoutDao;

import static WorkoutDB.WorkoutDao.Properties.Time;
import static WorkoutDB.WorkoutDao.Properties.WorkoutType;


/**
 * created by Alex Ivanov on 05.08.15.
 */
public class AppDatabase {

    private final WorkoutDao workoutDB;

    public AppDatabase(Context context) {
        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(context, "appDatabase", null);
        workoutDB = new DaoMaster(helper.getWritableDatabase()).newSession().getWorkoutDao();
    }

    public void insertWorkout(Workout Workout) {
        workoutDB.insertOrReplace(Workout);
    }

    public List<Workout> getAllWorkoutByType(int type) {
        return workoutDB.queryBuilder().where(WorkoutType.eq(type)).orderDesc(Time).list();
    }

    public List<Workout> getByDate(Date startDay, int type) {
        long endDay = DateUtils.getEndOfDay(startDay);
        return workoutDB.queryBuilder().where(Time.ge(startDay.getTime()
        ), Time.le(endDay), WorkoutType.eq(type)).orderDesc(Time).list();
    }

    public void clearStatsByType(int type) {
        workoutDB.deleteInTx(getAllWorkoutByType(type));
    }
}
