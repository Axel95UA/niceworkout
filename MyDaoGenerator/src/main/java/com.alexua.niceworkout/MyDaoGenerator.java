package com.alexua.niceworkout;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyDaoGenerator {

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(1, "WorkoutDB");

        Entity workout = schema.addEntity("Workout");
        workout.addIdProperty().autoincrement();
        workout.addIntProperty("workoutType");
        workout.addLongProperty("time").notNull();
        workout.addIntProperty("count").notNull();
        workout.addFloatProperty("speed");

        schema.enableKeepSectionsByDefault();

        new DaoGenerator().generateAll(schema, args[0]);
    }
}
